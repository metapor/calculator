import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// ngmodel 쓸려고 임포트
import { FormsModule } from '@angular/forms';
import { AppRoutingModule, RoutingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { CalV2Component } from './cal-v2/cal-v2.component';
import { CalV3Component } from './cal-v3/cal-v3.component';
import { TodoComponent } from './todo/todo.component';
import { TodoService } from './services/todo.service';
import { TodoItemComponent } from './todo/todo-item/todo-item.component';

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    CalV2Component,
    RoutingComponent,
    CalV3Component,
    TodoComponent,
    TodoItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    TodoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
