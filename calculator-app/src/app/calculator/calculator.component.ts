import { Component } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent {
    public num1: number;
    public num2: number;
    public result: number;
    public resultheading: any;

    sum() {
      this.result = this.num1 + this.num2;
      this.resultheading = this.num1 + ' + ' + this.num2;
    }
    diff() {
      this.result = this.num1 - this.num2;
      this.resultheading = this.num1 + ' - ' + this.num2;
    }
    mult() {
      /*
      this.result = this.num1 * this.num2;
      this.resultheading = this.num1 + ' x ' + this.num2;
      */
      let temp1 = 0;
      for (let i = 0; i < this.num2; i++) {
        temp1 += this.num1;
      }
      this.result = temp1;
      this.resultheading = this.num1 + ' x ' + this.num2;
    }
    div() {
      /*
      this.result = this.num1 / this.num2;
      this.resultheading = this.num1 + ' / ' + this.num2;
      */
      this.resultheading = this.num1 + ' / ' + this.num2;
      let count = 0;
      let temp = this.num1;
      while ( temp > 0 ) {
        temp -= this.num2;
        count++;
        console.log(count);
      }
      this.result = count;
    }
}
