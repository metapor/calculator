import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalculatorComponent } from './calculator/calculator.component';
import { CalV2Component } from './cal-v2/cal-v2.component';
import { CalV3Component } from './cal-v3/cal-v3.component';
import { TodoComponent } from './todo/todo.component';

const routes: Routes = [
  {path: 'cal', component: CalculatorComponent},
  {path: 'cal2', component: CalV2Component},
  {path: 'cal3', component: CalV3Component},
  {path: 'todo', component: TodoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const RoutingComponent = [];
