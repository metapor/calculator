import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalV3Component } from './cal-v3.component';

describe('CalV3Component', () => {
  let component: CalV3Component;
  let fixture: ComponentFixture<CalV3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalV3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalV3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
