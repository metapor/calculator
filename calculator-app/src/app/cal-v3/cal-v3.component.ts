import { Component } from '@angular/core';

@Component({
  selector: 'app-cal-v3',
  templateUrl: './cal-v3.component.html',
  styleUrls: ['./cal-v3.component.css']
})
export class CalV3Component {
  // 선언부
  input = ''; // 다항식 수식이 들어가는 곳
  output = null; // 입력 값 또는 결과 값이 들어가는 곳
  numberOutput = ''; // 숫자 출력
  result: any;
  numArray: string[] = [];

  calculate(input: Array<string>) {
    let arraySum = parseFloat(input[0]); // 초기값은 첫번째 배열
    for (let i = 0; i < input.length - 1; i++ ) {
      if ( input[i] === '+' ) {
        arraySum += parseFloat(input[i + 1]);
      } else if ( input[i] === '-' ) {
        arraySum -= parseFloat(input[i + 1]);
      } else if ( input[i] === '*' ) {
        arraySum *= parseFloat(input[i + 1]);
      } else if ( input[i] === '/' ) {
        arraySum /= parseFloat(input[i + 1]);
      }
    }
    return arraySum;
  }
  pressKey(key: string) {
    if ( key === '/' || key === '*' || key === '-' || key === '+') {
      // 연산자가 들어왔을 때
      const lastKey = this.input[this.input.length - 1]; // 수식의 마지막 글자 받아오기
      // console.log('key: ', key, '라스트키:', lastKey);
      if (lastKey === '/' || lastKey === '*' || lastKey === '-' || lastKey === '+') {
        if (key === lastKey) {
          // 연산자가 연속적으로 입력되었을 때 여기로 들어옴
          return;
        } else if (key !== lastKey) {
          // 키랑 라스트키랑 다른경우
          this.input = this.input.substr(0, this.input.length - 1);

          this.numArray.pop();
          this.numArray.push(key); // 배열에 들어있는 연산자를 빼고 새로 입력한 연산자를 넣음

          this.input += key;
          // console.log('짤라냈냐', this.input);
        }
      } else if (this.input === '') {
        // 연속으로 입력되있거나 비어있으면 연산자 입력 불가능
        return;
      } else {
        console.log(this.numberOutput);
        this.numArray.push(this.numberOutput); // 배열에 숫자 넣기
        this.numArray.push(key); // 배열에 연산자 넣기
        this.numberOutput = ''; // 연산자 누르면 숫자 초기화
        // console.log(this.numArray);
        this.input += key;
        console.log(this.numArray);
        this.result = this.calculate(this.numArray);
        // console.log(this.result);
        // console.log(typeof this.result);
        this.output = this.result;
      }
    } else {
      // 숫자가 들어왔을 때
      this.input += key;
      this.numberOutput += key;
      this.output = parseFloat(this.numberOutput);
    }
  }
  allClear() {
    // 초기화
    this.numArray = [];
    this.input = '';
    this.output = null;
    this.numberOutput = '';
  }
  getAnswer() {
    const lastKey = this.input[this.input.length - 1]; // 수식의 마지막 글자 받아오기
    if (this.input.length < 1) {
      this.output = '값을 입력하세요';
    } else if (lastKey === '/' || lastKey === '*' || lastKey === '-' || lastKey === '+') {
      // 연산자가 연속적으로 입력되었을 때 여기로 들어옴
      this.output = '숫자를 끝까지 입력하세요';
    } else {
    this.numArray.push(this.numberOutput); // 배열에 숫자 넣기
    // console.log(this.numArray);
    this.output = this.calculate(this.numArray);
    // console.log(this.output);
    }
  }
}
