import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalV2Component } from './cal-v2.component';

describe('CalV2Component', () => {
  let component: CalV2Component;
  let fixture: ComponentFixture<CalV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
