import { Component } from '@angular/core';

@Component({
  selector: 'app-cal-v2',
  templateUrl: './cal-v2.component.html',
  styleUrls: ['./cal-v2.component.css']
})
export class CalV2Component {
  mainText = ''; // 앞에 나타나는 글자
  subText = ''; // 뒤에 나타나는 글자
  operand1: number; // 첫번째 연산자
  operand2: number; // 두번째 연산자
  operator = ''; // 연산기
  calculationString = ''; // 에러출력
  answered = false;
  // 반복문 정답인지 아닌지 체크용
  operatorSet = false;
  // 이미 연산자가 입력되어있을 때 true로 바뀜
  pressKey(key: string) {
    if ( key === '/' || key === 'x' || key === '-' || key === '+') {
      // 연산자 입력시
      const lastKey = this.mainText[this.mainText.length - 1]; // 마지막으로 입력받은 연산자
      if (lastKey === '/' || lastKey === 'x' || lastKey === '-' || lastKey === '+') {
        // 연산자가 연속적으로 입력되지 않게
        this.operatorSet = true;
      }
      if ((this.operatorSet) || (this.mainText === '')) {
        return;
        // 처음엔 연산자가 들어가있거나 비어있으면 안됨
      }
      this.operand1 = parseFloat(this.mainText);
      this.operator = key;
      this.operatorSet = true;
    }
    if (this.mainText.length === 10) {
      return;
      // 자리수 10자 제한
    }
    this.mainText += key;
  }
  // =버튼 눌렀을 시에 출력 및 에러처리
  getAnswer() {
    this.calculationString = this.mainText;
    this.operand2 = parseFloat(this.mainText.split(this.operator)[1]);
    if (this.operator === '/') {
      this.subText = this.mainText;
      this.mainText = (this.operand1 / this.operand2).toString();
      this.subText = this.calculationString;
      if (this.mainText.length > 9) {
        this.mainText = this.mainText.substr(0, 9);
      }
    } else if (this.operator === 'x') {
      this.subText = this.mainText;
      this.mainText = (this.operand1 * this.operand2).toString();
      this.subText = this.calculationString;
      if (this.mainText.length > 9) {
        this.mainText = '에러';
        this.subText = '범위 초과';
      }
    } else if (this.operator === '-') {
      this.subText = this.mainText;
      this.mainText = (this.operand1 - this.operand2).toString();
      this.subText = this.calculationString;
    } else if (this.operator === '+') {
      this.subText = this.mainText;
      this.mainText = (this.operand1 + this.operand2).toString();
      this.subText = this.calculationString;
      if (this.mainText.length > 9) {
        this.mainText = '에러';
        this.subText = '범위 초과';
      }
    } else {
      this.subText = '알수없는 연산자';
    }
    this.answered = true;
  }
  allClear() {
    // 초기화
    this.mainText = '';
    this.subText = '';
    this.operator = '';
    this.calculationString = '';
    this.answered = false;
    this.operatorSet = false;
  }

}
